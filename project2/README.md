###Love/Hate Policy
`lastUsed[]` and `timeStamp` records the last time a frame is pinned or newed.  
A heap is maintained for all frames with `pin_cnt=0`, larger time first.  
`Loved[]` records whether the page in a frame is once loved.  
For a loved page, its time in the heap is negative.  
Once some frame has `pin_cnt=0` it will be inserted in to heap.  
When popping, additional checking is needed since a frame can be pinned again.
