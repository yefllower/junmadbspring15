///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"
#include <queue>


#define NUMBUF 20   
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.


/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  { 
	NO_FREE_FRAME,
	PAGE_NOT_FOUND,
	PAGE_NOT_PINNED,
	PAGE_PINNED
};

class Replacer;

class BufMgr {

private: // fill in this area
	struct Descriptor {
		PageId page_number;
		int pin_count;
		bool dirtybit;
	};
	Descriptor* bufDescr;

	struct ListNode {
		PageId page_number;
		int frame_number;
		ListNode *next;
	} **HashTable;

	int A, B;
	PageId Hash(PageId x) {
		return (A * x + B) % HTSIZE;
	}

	int findPage(PageId pageId) ;
	void insertPage(PageId pageId, int frameId) ;
	void deletePage(PageId pageId) ;

	bool* Loved;
	int timeStamp;
	int* lastUsed;
	priority_queue<pair<int, int> > Q;

	int getValue(int frameId) ;
	Status pickVictim(int &frameId) ;

public:

    Page* bufPool; // The actual DUMB PRIVATE buffer pool

    BufMgr (int Numbuf, Replacer *replacer = 0); 
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full 
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk

    Status pinPage(PageId pageId, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId pageId, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& pageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

    Status freePage(PageId pageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

    Status flushPage(PageId pageId);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */    
    Status unpinPage(PageId pageId, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(pageId, dirty, FALSE);
    }
};

#endif
