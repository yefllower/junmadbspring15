/*****************************************************************************/
/*************** Implementation of the Buffer Manager Layer ******************/
/*****************************************************************************/


#include "buf.h"


// Define buffer manager error messages here
//enum bufErrCodes  {...};

// Define error message here
static const char* bufErrMsgs[] = { 
  // error message strings go here
  "no frame is available, all frames are pinned",//NO_FREE_FRAME
  "page not found", //PAGE_NOT_FOUND
  "page not pinned", //PAGE_NOT_PINNED
  "page is still pinned", //PAGE_PINNED
};

// Create a static "error_string_table" object and register the error messages
// with minibase system 
static error_string_table bufTable(BUFMGR,bufErrMsgs);

int BufMgr::getValue(int frame_number) {
	int y = lastUsed[frame_number];
	if (Loved[frame_number])
		y = -y;
	return y;
}

int BufMgr::findPage(PageId pageId) {
	int tmp = Hash(pageId);
	ListNode *p = HashTable[tmp];
	int cnt = 0;
	while (p != NULL) {
		if (p->page_number == pageId)
			return p->frame_number;
		p = p->next;
		if (++cnt == 10) while(1);
	}
	return -1;
}
void BufMgr::insertPage(PageId pageId, int frameId) {
	int tmp = Hash(pageId);
	ListNode *p = HashTable[tmp];
	HashTable[tmp] = (ListNode*)malloc(sizeof(ListNode));
	HashTable[tmp]->page_number = pageId;
	HashTable[tmp]->frame_number = frameId;
	HashTable[tmp]->next = p;
}

void BufMgr::deletePage(PageId pageId) {
	int tmp = Hash(pageId);
	ListNode **p = HashTable + tmp;
	while ((*p) != NULL && (*p)->page_number != pageId) {
		p = &((*p)->next);
	}
	if ((*p) != NULL) {
		ListNode *q = *p;
		*p = (*p)->next;
		free(q);
	}
}

Status BufMgr::pickVictim(int &frameId) {
	while (!Q.empty()) {
		int x = Q.top().first;
		int i = Q.top().second;
		Q.pop();
		if (bufDescr[i].pin_count != 0)
			continue;
		if (x != getValue(i)) 
			continue;
		if (bufDescr[i].dirtybit) {
			Status st = MINIBASE_DB->write_page(bufDescr[i].page_number, bufPool + i);
			if (st != OK)
				return MINIBASE_CHAIN_ERROR(BUFMGR, st);
		}
		if (bufDescr[i].page_number != -1)
			deletePage(bufDescr[i].page_number);
		frameId = i;
		return OK;
	}
	return MINIBASE_FIRST_ERROR(BUFMGR, NO_FREE_FRAME);
}

BufMgr::BufMgr (int numbuf, Replacer *replacer) {
	// put your code here
	bufPool = (Page*) malloc(numbuf * sizeof(Page));
	bufDescr = (Descriptor*) malloc(numbuf * sizeof(Descriptor));
	Loved = (bool*) malloc(numbuf * sizeof(bool));
	lastUsed = (int*) malloc(numbuf * sizeof(int));
	HashTable = (ListNode**) malloc(HTSIZE * sizeof(ListNode*));
	timeStamp = 0;
	for (int i = 0; i < numbuf; i++) {
		bufDescr[i].page_number = -1;
		bufDescr[i].pin_count = 0;
		bufDescr[i].dirtybit = false;
		Loved[i] = false;
		lastUsed[i] = 1 << 30;
		Q.push(make_pair(getValue(i), i));
	}
	for (int i = 0; i < HTSIZE; i++)
		HashTable[i] = NULL;

	A = B = 0; 
	while (A == 0 || B == 0) {
		A = rand() % HTSIZE;
		B = rand() % HTSIZE;
	}
}

Status BufMgr::pinPage(PageId pageId, Page*& page, int emptyPage) {
	// put your code here
	int frame_number = findPage(pageId);
	if (frame_number == -1) {
		Status st = pickVictim(frame_number);
		if (st != OK)
			return MINIBASE_CHAIN_ERROR(BUFMGR, st);
		st = MINIBASE_DB->read_page(pageId, bufPool + frame_number);
		if (st != OK)
			return MINIBASE_CHAIN_ERROR(BUFMGR, st);
		insertPage(pageId, frame_number);
		bufDescr[frame_number].page_number = pageId;
		bufDescr[frame_number].dirtybit = false;
		bufDescr[frame_number].pin_count = 0;
		Loved[frame_number] = false;
	}
	lastUsed[frame_number] = timeStamp++;
	bufDescr[frame_number].pin_count++;
	page = bufPool + frame_number;
	return OK;
}//end pinPage


Status BufMgr::newPage(PageId& pageId, Page*& firstpage, int howmany) {
	// put your code here
	int frame_number;
	Status st = pickVictim(frame_number);
	if (st != OK)
		return MINIBASE_CHAIN_ERROR(BUFMGR, st);
	Q.push(make_pair(getValue(frame_number), frame_number));
	st = MINIBASE_DB->allocate_page(pageId, howmany);
	if (st != OK)
		return MINIBASE_CHAIN_ERROR(BUFMGR, st);
	insertPage(pageId, frame_number);
	bufDescr[frame_number].page_number = pageId;
	bufDescr[frame_number].dirtybit = false;
	bufDescr[frame_number].pin_count = 1;
	Loved[frame_number] = false;
	lastUsed[frame_number] = timeStamp++;
	firstpage = bufPool + frame_number;
	return OK;
}

Status BufMgr::flushPage(PageId pageId) {
	// put your code here
	int frame_number = findPage(pageId);
	if (frame_number == -1)
		return MINIBASE_FIRST_ERROR(BUFMGR, PAGE_NOT_FOUND);
	if (bufDescr[frame_number].dirtybit) {
		Status st = MINIBASE_DB->write_page(pageId, bufPool + frame_number);
		if (st != OK)
			return MINIBASE_CHAIN_ERROR(BUFMGR, st);
		bufDescr[frame_number].dirtybit = false;
	}
	return OK;
}


//*************************************************************
//** This is the implementation of ~BufMgr
//************************************************************
BufMgr::~BufMgr(){
	// put your code here
	flushAllPages();
	for (int i = 0; i < HTSIZE; i++) {
		ListNode* p = HashTable[i];
		ListNode* q;
		while (p != NULL) {
			q = p->next;
			free(p);
			p = q;
		}
	}
	free(bufDescr);
	free(Loved);
	free(lastUsed);
	free(bufPool);
	free(HashTable);
	while (!Q.empty()) Q.pop();
}


//*************************************************************
//** This is the implementation of unpinPage
//************************************************************

Status BufMgr::unpinPage(PageId pageId, int dirty=FALSE, int hate = FALSE){
	// put your code here
	int frame_number = findPage(pageId);
	if (frame_number == -1)
		return MINIBASE_FIRST_ERROR(BUFMGR, PAGE_NOT_FOUND);
	if (bufDescr[frame_number].pin_count == 0)
		return MINIBASE_FIRST_ERROR(BUFMGR, PAGE_NOT_PINNED);
	Loved[frame_number] |= !hate;
	bufDescr[frame_number].dirtybit |= dirty;
	bufDescr[frame_number].pin_count--;
	if (bufDescr[frame_number].pin_count == 0) 
		Q.push(make_pair(getValue(frame_number), frame_number));
	return OK;
}

//*************************************************************
//** This is the implementation of freePage
//************************************************************

Status BufMgr::freePage(PageId pageId){
	// put your code here	
	int frame_number = findPage(pageId);
	if (frame_number == -1)
		return MINIBASE_FIRST_ERROR(BUFMGR, PAGE_NOT_FOUND);
	if (bufDescr[frame_number].pin_count != 0)
		return MINIBASE_FIRST_ERROR(BUFMGR, PAGE_PINNED);
	Status st = MINIBASE_DB->deallocate_page(pageId);
	if (st != OK)
		return MINIBASE_CHAIN_ERROR(BUFMGR, st);
	deletePage(pageId);
	Loved[frame_number] = false;
	lastUsed[frame_number] = 1 << 30;
	Q.push(make_pair(getValue(frame_number), frame_number));
	return OK;
}

Status BufMgr::flushAllPages(){
	//put your code here
	for (int i = 0; i < HTSIZE; i++) {
		ListNode* p = HashTable[i];
		while (p != NULL) {
			if (bufDescr[p->frame_number].dirtybit) {
				Status st = MINIBASE_DB->write_page(p->page_number, bufPool + p->frame_number);
				if (st != OK)
					return MINIBASE_CHAIN_ERROR(BUFMGR, st);
				bufDescr[p->frame_number].dirtybit = false;
			}
			p = p->next;
		}
	}
	return OK;
}
